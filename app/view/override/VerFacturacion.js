Ext.define('BTC.view.override.VerFacturacion', {
    override: 'BTC.view.VerFacturacion',
    
    initComponent: function(){
        var me = this;

        Ext.applyIf(me, {
            features: [{
                ftype: 'filters',
                menuFilterText: 'Filtros',
                //encode: false,
                local: true,
                filters: [{
                    type: 'numeric',
                    dataIndex: 'folio'
                },{
                    type: 'date',
                    dataIndex: 'fecha_captura'
                },{
                    type: 'string',
                    dataIndex: 'cliente'
                },{
                    type: 'string',
                    dataIndex: 'sucursal'
                },{
                    type: 'numeric',
                    dataIndex: 'subtotal'
                },{
                    type: 'numeric',
                    dataIndex: 'total'
                },{
                    type: 'boolean',
                    dataIndex: 'cancelado',
                    yesText: 'Si',
                    noText: 'No'
                }]
            }]
        });

        me.callParent(arguments);
    }
    
});