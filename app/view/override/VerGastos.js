Ext.define('BTC.view.override.VerGastos', {
    override: 'BTC.view.VerGastos',
    
    initComponent: function(){
        var me = this;

        Ext.applyIf(me, {
            features: [{
                ftype: 'filters',
                menuFilterText: 'Filtros',
                //encode: false,
                local: true,
                filters: [{
                    type: 'numeric',
                    dataIndex: 'id'
                },{
                    type: 'string',
                    dataIndex: 'sucursal'
                },{
                    type: 'boolean',
                    dataIndex: 'borrado',
                    yesText: 'Si',
                    noText: 'No',
                },{
                    type: 'date',
                    dataIndex: 'fecha'
                },{
                    type: 'numeric',
                    dataIndex: 'monto'
                },{
                    type: 'string',
                    dataIndex: 'cuenta_contable'
                }]
            }]
        });

        me.callParent(arguments);
    }
    
});