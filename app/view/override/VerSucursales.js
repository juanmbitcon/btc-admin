Ext.define('BTC.view.override.VerSucursales', {
    override: 'BTC.view.VerSucursales',
    
    initComponent: function(){
        var me = this;

        Ext.applyIf(me, {
            features: [{
                ftype: 'filters',
                menuFilterText: 'Filtros',
                //encode: false,
                local: true,
                filters: [{
                    type: 'numeric',
                    dataIndex: 'id'
                },{
                    type: 'string',
                    dataIndex: 'nombre_sucursal'
                },{
                    type: 'boolean',
                    dataIndex: 'borrado',
                    yesText: 'Si',
                    noText: 'No',
                },{
                    type: 'string',
                    dataIndex: 'colonia'
                },{
                    type: 'string',
                    dataIndex: 'localidad'
                },{
                    type: 'string',
                    dataIndex: 'municipio'
                },{
                    type: 'numeric',
                    dataIndex: 'codigo_postal'
                },{
                    type: 'string',
                    dataIndex: 'email'
                }]
            }]
        });

        me.callParent(arguments);
    }
    
});