Ext.define('BTC.view.override.VerRecibos', {
    override: 'BTC.view.VerRecibos',
    
    initComponent: function(){
        var me = this;

        Ext.applyIf(me, {
            features: [{
                ftype: 'filters',
                menuFilterText: 'Filtros',
                //encode: false,
                local: true,
                filters: [{
                    type: 'numeric',
                    dataIndex: 'id'
                },{
                    type: 'string',
                    dataIndex: 'folio_fisico'
                },{
                    type: 'boolean',
                    dataIndex: 'borrado',
                    yesText: 'Si',
                    noText: 'No',
                },{
                    type: 'string',
                    dataIndex: 'cliente'
                },{
                    type: 'string',
                    dataIndex: 'sucursal'
                },{
                    type: 'numeric',
                    dataIndex: 'sesion'
                },{
                    type: 'numeric',
                    dataIndex: 'monto'
                },{
                    type: 'boolean',
                    dataIndex: 'facturado',
                    yesText: 'Si',
                    noText: 'No'
                },{
                    type: 'date',
                    dataIndex: 'fecha_captura'
                }]
            }]
        });

        me.callParent(arguments);
    }
    
});