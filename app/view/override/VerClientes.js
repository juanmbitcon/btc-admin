Ext.define('BTC.view.override.VerClientes', {
    override: 'BTC.view.VerClientes',
    
    initComponent: function(){
        var me = this;

        Ext.applyIf(me, {
            features: [{
                ftype: 'filters',
                menuFilterText: 'Filtros',
                //encode: false,
                local: true,
                filters: [{
                    type: 'numeric',
                    dataIndex: 'id'
                },{
                    type: 'string',
                    dataIndex: 'nombre_completo'
                },{
                    type: 'boolean',
                    dataIndex: 'borrado',
                    yesText: 'Si',
                    noText: 'No',
                },{
                    type: 'string',
                    dataIndex: 'email'
                },{
                    type: 'numeric',
                    dataIndex: 'telefono'
                },{
                    type: 'numeric',
                    dataIndex: 'celular'
                },{
                    type: 'numeric',
                    dataIndex: 'edad'
                },{
                    type: 'numeric',
                    dataIndex: 'peso'
                },{
                    type: 'numeric',
                    dataIndex: 'estatura'
                },{
                    type: 'string',
                    dataIndex: 'sexo'
                },{
                    type: 'list',
                    dataIndex: 'tipo_ejercicio',
                    options: [
                        'FITNESS',
                        'CAMINATA',
                        'CORRER',
                        'FUTBOL',
                        'BASQUETBOL',
                        'OTRO'
                    ]
                },{
                    type: 'list',
                    dataIndex: 'tipo_lesion',
                    options: [
                        'GONARTROSIS I',
                        'GONARTROSIS II',
                        'GONARTROSIS III',
                        'GONARTROSIS IV',
                        'GONARTROSIS V',
                        'OTRO'
                    ]
                },{
                    type: 'list',
                    dataIndex: 'ocupacion',
                    options: [
                          'EMPLEADO ASALARIADO',
                          'AUTOEMPLEADO, NEGOCIO PROPIO',
                          'DESEMPLEADO',
                          'ESTUDIANTE',
                          'LABORES DEL HOGAR',
                          'MILITAR',
                          'RETIRADO',
                          'NO PUEDE TRABAJAR'
                         ]
                },{
                    type: 'list',
                    dataIndex: 'escolaridad',
                    options: [
                        'PRIMARIA',
                        'SECUNDARIA',
                        'PREPARATORIA',
                        'ESCUELA TECNICA',
                        'UNIVERSIDAD',
                        'POSGRADO',
                        'DOCTORADO',
                        'OTRO'
                    ]
                },{
                    type: 'list',
                    dataIndex: 'estatus_marital',
                    options: [
                        'SOLTERO, NUNCA CASADO',
                        'CASADO',
                        'UNION LIBRE',
                        'VIUDO',
                        'DIVORCIADO, SEPARADO'
                    ]
                },{
                    type: 'list',
                    dataIndex: 'tipo_vivienda',
                    options: [
                        'CASA INDEPENDIENTE',
                        'DEPTO EN EDIFICIO',
                        'OTRO'
                    ]
                }]
            }]
        });

        me.callParent(arguments);
    }
    
    
});