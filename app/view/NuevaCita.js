/*
 * File: app/view/NuevaCita.js
 *
 * This file was generated by Sencha Architect version 3.2.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('BTC.view.NuevaCita', {
    extend: 'Ext.window.Window',
    alias: 'widget.nuevacita',

    requires: [
        'Ext.form.Panel',
        'Ext.toolbar.Toolbar',
        'Ext.form.Label',
        'Ext.form.field.Hidden',
        'Ext.form.FieldContainer',
        'Ext.form.field.Display',
        'Ext.form.field.ComboBox',
        'Ext.form.field.TextArea',
        'Ext.toolbar.Fill',
        'Ext.button.Button'
    ],

    height: 620,
    itemId: 'NuevaCita',
    width: 750,
    resizable: false,
    layout: 'fit',
    title: 'NUEVA CITA',
    titleAlign: 'center',
    modal: true,

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            style: {
                border: 'none'
            },
            items: [
                {
                    xtype: 'form',
                    layout: 'border',
                    dockedItems: [
                        {
                            xtype: 'toolbar',
                            flex: 1,
                            dock: 'top',
                            ui: 'footer',
                            items: [
                                {
                                    xtype: 'label',
                                    flex: 1,
                                    padding: '3 0',
                                    style: {
                                        'font-size': '18px',
                                        'text-align': 'center',
                                        'line-height': '20px'
                                    },
                                    text: 'Horario y Fecha de la Cita'
                                }
                            ]
                        }
                    ],
                    items: [
                        {
                            xtype: 'container',
                            region: 'north',
                            padding: '5 15',
                            style: {
                                'background-color': '#dfeaf2'
                            },
                            layout: {
                                type: 'hbox',
                                align: 'stretch'
                            },
                            items: [
                                {
                                    xtype: 'hiddenfield',
                                    flex: 1,
                                    fieldLabel: 'Label',
                                    name: 'id_horario_atencion'
                                },
                                {
                                    xtype: 'hiddenfield',
                                    flex: 1,
                                    fieldLabel: 'Label',
                                    name: 'id_sucursal'
                                },
                                {
                                    xtype: 'fieldcontainer',
                                    flex: 1,
                                    width: 400,
                                    layout: 'anchor',
                                    items: [
                                        {
                                            xtype: 'fieldcontainer',
                                            layout: {
                                                type: 'hbox',
                                                align: 'stretch'
                                            },
                                            items: [
                                                {
                                                    xtype: 'displayfield',
                                                    flex: 1,
                                                    fieldLabel: 'Sucursal',
                                                    labelCls: 'label-bold',
                                                    name: 'sucursal'
                                                },
                                                {
                                                    xtype: 'label',
                                                    html: '<span class="font-awesome">&#xf0f8;</span>',
                                                    style: {
                                                        'font-size': '15px',
                                                        'line-height': '25px',
                                                        'text-align': 'center',
                                                        margin: '0 3px'
                                                    },
                                                    width: '20px'
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'fieldcontainer',
                                            layout: {
                                                type: 'hbox',
                                                align: 'stretch'
                                            },
                                            items: [
                                                {
                                                    xtype: 'displayfield',
                                                    flex: 1,
                                                    fieldLabel: 'Fecha',
                                                    labelCls: 'label-bold',
                                                    name: 'fecha',
                                                    submitValue: true
                                                },
                                                {
                                                    xtype: 'label',
                                                    html: '<span class="font-awesome">&#xf073;</span>',
                                                    style: {
                                                        'font-size': '15px',
                                                        'line-height': '25px',
                                                        'text-align': 'center',
                                                        margin: '0 3px'
                                                    },
                                                    width: '20px'
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    xtype: 'fieldcontainer',
                                    flex: 1,
                                    width: 400,
                                    layout: 'anchor',
                                    items: [
                                        {
                                            xtype: 'fieldcontainer',
                                            layout: {
                                                type: 'hbox',
                                                align: 'stretch'
                                            },
                                            items: [
                                                {
                                                    xtype: 'displayfield',
                                                    flex: 1,
                                                    fieldLabel: 'Inicio',
                                                    labelCls: 'label-bold',
                                                    name: 'inicio'
                                                },
                                                {
                                                    xtype: 'label',
                                                    html: '<span class="font-awesome">&#xf017;</span>',
                                                    style: {
                                                        'font-size': '15px',
                                                        'line-height': '25px',
                                                        'text-align': 'center',
                                                        margin: '0 3px'
                                                    },
                                                    width: '20px'
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'fieldcontainer',
                                            layout: {
                                                type: 'hbox',
                                                align: 'stretch'
                                            },
                                            items: [
                                                {
                                                    xtype: 'displayfield',
                                                    flex: 1,
                                                    fieldLabel: 'Fin',
                                                    labelCls: 'label-bold',
                                                    name: 'fin'
                                                },
                                                {
                                                    xtype: 'label',
                                                    html: '<span class="font-awesome">&#xf017;</span>',
                                                    style: {
                                                        'font-size': '15px',
                                                        'line-height': '25px',
                                                        'text-align': 'center',
                                                        margin: '0 3px'
                                                    },
                                                    width: '20px'
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            xtype: 'form',
                            region: 'center',
                            itemId: 'ClienteSeleccionado',
                            layout: {
                                type: 'vbox',
                                align: 'stretch',
                                padding: 5
                            },
                            items: [
                                {
                                    xtype: 'fieldcontainer',
                                    padding: 5,
                                    width: 400,
                                    layout: {
                                        type: 'hbox',
                                        align: 'stretch'
                                    },
                                    items: [
                                        {
                                            xtype: 'combobox',
                                            flex: 1,
                                            fieldLabel: 'Cliente',
                                            labelCls: 'label-bold',
                                            name: 'id_cliente',
                                            allowBlank: false,
                                            emptyText: 'Selecciona un Cliente',
                                            displayField: 'nombre_completo',
                                            forceSelection: true,
                                            queryMode: 'local',
                                            store: 'Clientes',
                                            typeAhead: true,
                                            typeAheadDelay: 150,
                                            valueField: 'id',
                                            listeners: {
                                                select: {
                                                    fn: me.onComboboxSelect,
                                                    scope: me
                                                }
                                            }
                                        },
                                        {
                                            xtype: 'label',
                                            html: '<span class="font-awesome">&#xf183;</span>',
                                            style: {
                                                'font-size': '15px',
                                                'line-height': '25px',
                                                'text-align': 'center',
                                                margin: '0 3px'
                                            },
                                            width: '20px'
                                        }
                                    ]
                                },
                                {
                                    xtype: 'container',
                                    flex: 1,
                                    layout: {
                                        type: 'hbox',
                                        align: 'stretch',
                                        padding: '10 10 0 10'
                                    },
                                    items: [
                                        {
                                            xtype: 'hiddenfield',
                                            flex: 1,
                                            fieldLabel: 'Label',
                                            name: 'nombre_completo'
                                        },
                                        {
                                            xtype: 'fieldcontainer',
                                            flex: 1,
                                            width: 400,
                                            layout: {
                                                type: 'vbox',
                                                align: 'stretch'
                                            },
                                            items: [
                                                {
                                                    xtype: 'fieldcontainer',
                                                    flex: 1,
                                                    layout: {
                                                        type: 'hbox',
                                                        align: 'stretch'
                                                    },
                                                    items: [
                                                        {
                                                            xtype: 'displayfield',
                                                            flex: 1,
                                                            fieldLabel: 'Nombre',
                                                            labelAlign: 'top',
                                                            labelCls: 'label-bold',
                                                            name: 'nombre'
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'fieldcontainer',
                                                    flex: 1,
                                                    layout: {
                                                        type: 'hbox',
                                                        align: 'stretch'
                                                    },
                                                    items: [
                                                        {
                                                            xtype: 'displayfield',
                                                            flex: 1,
                                                            fieldLabel: 'Apellido Paterno',
                                                            labelAlign: 'top',
                                                            labelCls: 'label-bold',
                                                            name: 'apellido_paterno'
                                                        },
                                                        {
                                                            xtype: 'displayfield',
                                                            flex: 1,
                                                            fieldLabel: 'Apellido Materno',
                                                            labelAlign: 'top',
                                                            labelCls: 'label-bold',
                                                            name: 'apellido_materno'
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'fieldcontainer',
                                                    flex: 1,
                                                    layout: {
                                                        type: 'hbox',
                                                        align: 'stretch'
                                                    },
                                                    items: [
                                                        {
                                                            xtype: 'displayfield',
                                                            flex: 1,
                                                            fieldLabel: 'Email',
                                                            labelAlign: 'top',
                                                            labelCls: 'label-bold',
                                                            name: 'email',
                                                            submitValue: true
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'fieldcontainer',
                                                    flex: 1,
                                                    layout: {
                                                        type: 'hbox',
                                                        align: 'stretch'
                                                    },
                                                    items: [
                                                        {
                                                            xtype: 'displayfield',
                                                            flex: 1,
                                                            fieldLabel: 'Teléfono',
                                                            labelAlign: 'top',
                                                            labelCls: 'label-bold',
                                                            name: 'telefono',
                                                            submitValue: true
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'fieldcontainer',
                                                    flex: 1,
                                                    layout: {
                                                        type: 'hbox',
                                                        align: 'stretch'
                                                    },
                                                    items: [
                                                        {
                                                            xtype: 'displayfield',
                                                            flex: 1,
                                                            fieldLabel: 'Celular',
                                                            labelAlign: 'top',
                                                            labelCls: 'label-bold',
                                                            name: 'celular'
                                                        }
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'fieldcontainer',
                                            flex: 1,
                                            width: 400,
                                            layout: {
                                                type: 'vbox',
                                                align: 'stretch'
                                            },
                                            items: [
                                                {
                                                    xtype: 'fieldcontainer',
                                                    flex: 1,
                                                    layout: {
                                                        type: 'hbox',
                                                        align: 'stretch'
                                                    },
                                                    items: [
                                                        {
                                                            xtype: 'displayfield',
                                                            flex: 1,
                                                            fieldLabel: 'RFC',
                                                            labelAlign: 'top',
                                                            labelCls: 'label-bold',
                                                            name: 'rfc'
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'fieldcontainer',
                                                    flex: 1,
                                                    layout: {
                                                        type: 'hbox',
                                                        align: 'stretch'
                                                    },
                                                    items: [
                                                        {
                                                            xtype: 'displayfield',
                                                            flex: 1,
                                                            fieldLabel: 'Calle',
                                                            labelAlign: 'top',
                                                            labelCls: 'label-bold',
                                                            name: 'calle'
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'fieldcontainer',
                                                    flex: 1,
                                                    layout: {
                                                        type: 'hbox',
                                                        align: 'stretch'
                                                    },
                                                    items: [
                                                        {
                                                            xtype: 'displayfield',
                                                            flex: 1,
                                                            fieldLabel: 'Número Ext.',
                                                            labelAlign: 'top',
                                                            labelCls: 'label-bold',
                                                            name: 'numero_exterior'
                                                        },
                                                        {
                                                            xtype: 'displayfield',
                                                            flex: 1,
                                                            fieldLabel: 'Número Int.',
                                                            labelAlign: 'top',
                                                            labelCls: 'label-bold',
                                                            name: 'numero_interior'
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'fieldcontainer',
                                                    flex: 1,
                                                    layout: {
                                                        type: 'hbox',
                                                        align: 'stretch'
                                                    },
                                                    items: [
                                                        {
                                                            xtype: 'displayfield',
                                                            flex: 1,
                                                            fieldLabel: 'Páis',
                                                            labelAlign: 'top',
                                                            labelCls: 'label-bold',
                                                            name: 'pais'
                                                        },
                                                        {
                                                            xtype: 'displayfield',
                                                            flex: 1,
                                                            fieldLabel: 'Estado',
                                                            labelAlign: 'top',
                                                            labelCls: 'label-bold',
                                                            name: 'estado'
                                                        },
                                                        {
                                                            xtype: 'displayfield',
                                                            flex: 1,
                                                            fieldLabel: 'Municipio',
                                                            labelAlign: 'top',
                                                            labelCls: 'label-bold',
                                                            name: 'municipio_delegacion'
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'fieldcontainer',
                                                    flex: 1,
                                                    layout: {
                                                        type: 'hbox',
                                                        align: 'stretch'
                                                    },
                                                    items: [
                                                        {
                                                            xtype: 'displayfield',
                                                            flex: 1,
                                                            fieldLabel: 'Colonia',
                                                            labelAlign: 'top',
                                                            labelCls: 'label-bold',
                                                            name: 'colonia'
                                                        },
                                                        {
                                                            xtype: 'displayfield',
                                                            flex: 1,
                                                            fieldLabel: 'C.P.',
                                                            labelAlign: 'top',
                                                            labelCls: 'label-bold',
                                                            name: 'codigo_postal'
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    xtype: 'textareafield',
                                    formBind: true,
                                    hidden: true,
                                    padding: '5 20',
                                    fieldLabel: 'Comentarios',
                                    labelAlign: 'top',
                                    labelCls: 'label-bold',
                                    name: 'comentarios',
                                    emptyText: 'Comentarios Opcionales',
                                    enforceMaxLength: true,
                                    maxLength: 300
                                }
                            ],
                            dockedItems: [
                                {
                                    xtype: 'toolbar',
                                    flex: 1,
                                    dock: 'bottom',
                                    ui: 'footer',
                                    items: [
                                        {
                                            xtype: 'tbfill'
                                        },
                                        {
                                            xtype: 'button',
                                            handler: function(button, e) {
                                                var form = button.up('window').down('form'),
                                                    values = form.getForm().getValues(),
                                                    wdow = button.up('window'),
                                                    ctrl = BTC.app.getController('Citas');

                                                var estatus = new BTC.store.Estatus({autoLoad:false});
                                                estatus.load({
                                                    params: {
                                                        request: JSON.stringify([{field:'nombre_tabla',value:'cita'}])
                                                    },
                                                    callback: function(records, operation, success){
                                                        if (success){

                                                            var store = this;
                                                            Ext.Msg.show({
                                                                title: 'Creación de Cita',
                                                                msg: '¿Desea crear la cita?',
                                                                icon: Ext.Msg.QUESTION,
                                                                buttonText: {yes:'Si', no:'No'},
                                                                fn: function(btn){
                                                                    if (btn == 'yes'){
                                                                        ctrl.createCita({
                                                                            fecha: values.fecha,
                                                                            nombre_paciente: values.nombre_completo,
                                                                            email: values.email,
                                                                            telefono: values.telefono,
                                                                            comentarios: values.comentarios,
                                                                            id_sucursal: values.id_sucursal,
                                                                            id_horario_atencion: values.id_horario_atencion,
                                                                            id_estatus: store.findRecord('nombre','Confirmada').get('id'),
                                                                            id_cliente: values.id_cliente
                                                                        },function(){
                                                                            wdow.close();
                                                                            BTC.app.getController('Calendario').agenda.reload();
                                                                        },function(){
                                                                            wdow.close();
                                                                            BTC.app.getController('Calendario').agenda.reload();
                                                                        });
                                                                    }
                                                                }
                                                            });

                                                        } else {
                                                            BTC.app.showFatalError();
                                                        }
                                                    }
                                                });
                                            },
                                            formBind: true,
                                            glyph: 'xf0c7@FontAwesome',
                                            iconAlign: 'right',
                                            text: 'Guardar Cita'
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ],
            listeners: {
                show: {
                    fn: me.onNuevaCitaShow,
                    scope: me
                }
            }
        });

        me.callParent(arguments);
    },

    onComboboxSelect: function(combo, records, eOpts) {
        var cliente = combo.getStore().findRecord('id',combo.getValue());

        combo.up('#ClienteSeleccionado').loadRecord(cliente);
    },

    onNuevaCitaShow: function(component, eOpts) {
        Ext.getStore('Clientes').load();
    }

});