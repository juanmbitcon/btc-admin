/*
 * File: app/view/EmpleadoNuevoEditar.js
 *
 * This file was generated by Sencha Architect version 3.2.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('BTC.view.EmpleadoNuevoEditar', {
    extend: 'Ext.window.Window',
    alias: 'widget.empleadonuevoeditar',

    requires: [
        'Ext.form.Panel',
        'Ext.form.field.ComboBox',
        'Ext.XTemplate',
        'Ext.form.field.Date',
        'Ext.toolbar.Toolbar',
        'Ext.toolbar.Fill',
        'Ext.button.Button',
        'Ext.form.field.Display'
    ],

    itemId: 'EmpleadoNuevoEditar',
    width: 400,
    resizable: false,
    layout: 'card',
    title: '',
    titleAlign: 'center',
    modal: true,

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            style: {
                border: 'none'
            },
            items: [
                {
                    xtype: 'form',
                    itemId: 'NuevoEmpleado',
                    bodyPadding: 20,
                    items: [
                        {
                            xtype: 'combobox',
                            anchor: '100%',
                            afterLabelTextTpl: [
                                '<span class="required-field-label">*</span>'
                            ],
                            fieldLabel: 'Sucursal',
                            labelAlign: 'top',
                            labelCls: 'label-bold',
                            name: 'id_sucursal',
                            allowBlank: false,
                            displayField: 'nombre_sucursal',
                            forceSelection: true,
                            queryMode: 'local',
                            store: 'Sucursales',
                            typeAhead: true,
                            typeAheadDelay: 150,
                            valueField: 'id'
                        },
                        {
                            xtype: 'textfield',
                            anchor: '100%',
                            afterLabelTextTpl: [
                                '<span class="required-field-label">*</span>'
                            ],
                            fieldLabel: 'Nombre',
                            labelAlign: 'top',
                            labelCls: 'label-bold',
                            name: 'nombre',
                            allowBlank: false
                        },
                        {
                            xtype: 'textfield',
                            anchor: '100%',
                            afterLabelTextTpl: [
                                '<span class="required-field-label">*</span>'
                            ],
                            fieldLabel: 'Apellido Paterno',
                            labelAlign: 'top',
                            labelCls: 'label-bold',
                            name: 'apellido_paterno',
                            allowBlank: false
                        },
                        {
                            xtype: 'textfield',
                            anchor: '100%',
                            afterLabelTextTpl: [
                                '<span class="required-field-label">*</span>'
                            ],
                            fieldLabel: 'Apellido Materno',
                            labelAlign: 'top',
                            labelCls: 'label-bold',
                            name: 'apellido_materno',
                            allowBlank: false
                        },
                        {
                            xtype: 'datefield',
                            anchor: '100%',
                            afterLabelTextTpl: [
                                '<span class="required-field-label">*</span>'
                            ],
                            fieldLabel: 'Fecha de Nacimiento',
                            labelAlign: 'top',
                            labelCls: 'label-bold',
                            name: 'fecha_nacimiento',
                            allowBlank: false,
                            editable: false,
                            format: 'd/m/Y',
                            submitFormat: 'Y-m-d'
                        },
                        {
                            xtype: 'combobox',
                            anchor: '100%',
                            afterLabelTextTpl: [
                                '<span class="required-field-label">*</span>'
                            ],
                            fieldLabel: 'Puesto',
                            labelAlign: 'top',
                            labelCls: 'label-bold',
                            name: 'id_puesto_empleado',
                            allowBlank: false,
                            displayField: 'nombre',
                            forceSelection: true,
                            queryMode: 'local',
                            store: 'Puestos',
                            typeAhead: true,
                            typeAheadDelay: 150,
                            valueField: 'id'
                        }
                    ],
                    dockedItems: [
                        {
                            xtype: 'toolbar',
                            dock: 'bottom',
                            ui: 'footer',
                            items: [
                                {
                                    xtype: 'tbfill'
                                },
                                {
                                    xtype: 'button',
                                    formBind: true,
                                    itemId: 'BtnGuardarNvoEmpleado',
                                    glyph: 'xf0c7@FontAwesome',
                                    iconAlign: 'right',
                                    text: 'Guardar'
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'form',
                    itemId: 'EditarEmpleado',
                    bodyPadding: 10,
                    items: [
                        {
                            xtype: 'displayfield',
                            anchor: '100%',
                            fieldLabel: 'ID',
                            labelCls: 'label-bold',
                            name: 'id',
                            submitValue: true
                        },
                        {
                            xtype: 'displayfield',
                            anchor: '100%',
                            fieldLabel: 'Sucursal',
                            labelCls: 'label-bold',
                            name: 'sucursal'
                        },
                        {
                            xtype: 'textfield',
                            anchor: '100%',
                            afterLabelTextTpl: [
                                '<span class="required-field-label">*</span>'
                            ],
                            fieldLabel: 'Nombre',
                            labelAlign: 'top',
                            labelCls: 'label-bold',
                            name: 'nombre',
                            allowBlank: false
                        },
                        {
                            xtype: 'textfield',
                            anchor: '100%',
                            afterLabelTextTpl: [
                                '<span class="required-field-label">*</span>'
                            ],
                            fieldLabel: 'Apellido Paterno',
                            labelAlign: 'top',
                            labelCls: 'label-bold',
                            name: 'apellido_paterno',
                            allowBlank: false
                        },
                        {
                            xtype: 'textfield',
                            anchor: '100%',
                            afterLabelTextTpl: [
                                '<span class="required-field-label">*</span>'
                            ],
                            fieldLabel: 'Apellido Materno',
                            labelAlign: 'top',
                            labelCls: 'label-bold',
                            name: 'apellido_materno',
                            allowBlank: false
                        },
                        {
                            xtype: 'datefield',
                            anchor: '100%',
                            afterLabelTextTpl: [
                                '<span class="required-field-label">*</span>'
                            ],
                            fieldLabel: 'Fecha de Nacimiento',
                            labelAlign: 'top',
                            labelCls: 'label-bold',
                            name: 'fecha_nacimiento',
                            allowBlank: false,
                            editable: false,
                            format: 'd/m/Y',
                            submitFormat: 'Y-m-d'
                        },
                        {
                            xtype: 'combobox',
                            anchor: '100%',
                            afterLabelTextTpl: [
                                '<span class="required-field-label">*</span>'
                            ],
                            fieldLabel: 'Puesto',
                            labelAlign: 'top',
                            labelCls: 'label-bold',
                            name: 'id_puesto_empleado',
                            allowBlank: false,
                            displayField: 'nombre',
                            forceSelection: true,
                            queryMode: 'local',
                            store: 'Puestos',
                            typeAhead: true,
                            typeAheadDelay: 150,
                            valueField: 'id'
                        }
                    ],
                    dockedItems: [
                        {
                            xtype: 'toolbar',
                            dock: 'bottom',
                            ui: 'footer',
                            items: [
                                {
                                    xtype: 'tbfill'
                                },
                                {
                                    xtype: 'button',
                                    formBind: true,
                                    itemId: 'BtnGuardarCambiosEmpleado',
                                    glyph: 'xf0c7@FontAwesome',
                                    iconAlign: 'right',
                                    text: 'Guardar Cambios'
                                },
                                {
                                    xtype: 'button',
                                    handler: function(button, e) {
                                        button.up('window').close();
                                    },
                                    formBind: false,
                                    glyph: 'xf00d@FontAwesome',
                                    iconAlign: 'right',
                                    text: 'Cancelar'
                                }
                            ]
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});