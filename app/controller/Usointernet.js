/*
 * File: app/controller/Usointernet.js
 *
 * This file was generated by Sencha Architect version 3.2.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('BTC.controller.Usointernet', {
    extend: 'Ext.app.Controller',

    refs: [
        {
            ref: 'panelUinternet',
            selector: '#PanelUinternet'
        }
    ],

    onMenuClick: function(menu, item, e, eOpts) {
        var ctrl = this,
            item = item.getItemId(),
            uso = this.getSelectedRecord();


        switch (item){
             case 'editar':
               ctrl.editarUsointernet(uso);
                break;

            case 'eliminar':
                Ext.Msg.show({
                    title: '¿Desea continuar con el borrado?',
                    msg: 'Esta acción no se puede deshacer, ¿desea continuar con el borrado de la frecuencia de uso de internet '+uso.get('nombre')+'?',
                    buttonText: {yes: "Borrar",no: "Cancelar"},
                    icon: Ext.Msg.QUESTION,
                    fn: function(btn){
                        if (btn == 'yes'){
                            ctrl.borrarUinternet(uso.get('id'));
                        }
                    }
                });
                break;
        }

    },

    onExitEdition: function(button, e, eOpts) {
        var form = button.up('form'),
            panelUinternet = this.getPanelUinternet(),
            pnuevo = panelUinternet.down('#PanelNuevo'),
            gridView =form.up('grid').getView();


            form.getForm().reset();
            gridView.enable();
            pnuevo.up('panel').getLayout().setActiveItem(pnuevo);

    },

    onCreateNewUinternet: function(button, e, eOpts) {
        var form = button.up('form'),
            grid = button.up('grid'),
            values = form.getForm().getValues();

        Ext.data.JsonP.request({
            url: BTC.app.base_url,
            scope: this,
            params: {
                c: 'perfildemografico',
                m: 'createUsoInternet',
                request: JSON.stringify(values)
            },
            success: function(response){
                if (response.success){
                    BTC.app.showDialog('Frecuencia de uso de internet Creado', response.response, 'xf087', 'right', false, 3000);
                    form.getForm().reset();
                    grid.getStore().reload();
                } else {
                    Ext.Msg.alert('Error', response.response);
                }
            },
            failure: function(response){
                BTC.app.showFatalError();
            }
        });
    },

    onGuardarCambiosUinternet: function(button, e, eOpts) {
        var internet = this.getSelectedRecord(),
            form = button.up('form'),
            values = form.getForm().getValues(),
            grid = button.up('grid'),
            panelU = this.getPanelUinternet(),
            pnuevo = panelU.down('#PanelNuevo'),
            request = {
                id: internet.get('id'),
                data: values
            };

        Ext.data.JsonP.request({
            url: BTC.app.base_url,
            params: {
                c: 'perfildemografico',
                m: 'editUsoInternet',
                request: JSON.stringify(request)
            },
            scope: this,
            success: function(response){
                if (response.success){
                    BTC.app.showDialog('Frecuencia de uso de internet Editado', response.response, 'xf087', 'right', false, 3000);
                    grid.getStore().reload();
                    form.getForm().reset();
                    pnuevo.up('panel').getLayout().setActiveItem(pnuevo);

                } else {
                    Ext.Msg.alert('Error', response.response);
                }
            },
            failure: function(response){
                BTC.app.showFatalError();
            }
        });
    },

    showContextMenu: function(record, xy) {
        this.selectedRecord = record;

        var menu = Ext.create('Ext.menu.Menu', {
            width: 120,
            bodyPadding: 3,
            autoDestroy: true,
            itemId: 'UintContextMenu',
            items: [{
                xtype: 'menuitem',
                glyph: 'xf044@FontAwesome',
                itemId: 'editar',
                //disabled: !perms.findAccionByName(vista, 'editar'),
                text: 'Editar'
            },{
                xtype: 'menuseparator'
            },{
                xtype: 'menuitem',
                glyph: 'xf1f8@FontAwesome',
                itemId: 'eliminar',
                //disabled: !perms.findAccionByName(vista, 'eliminar'),
                text: 'Eliminar'
            }]
        });

        menu.showAt(xy[0], xy[1]);
    },

    getSelectedRecord: function() {
        return this.selectedRecord;
    },

    editarUsointernet: function(uso) {
        var puso = this.getPanelUinternet(),
            peditar = puso.down('#PanelEditar'),
            gridView = peditar.up('grid').getView();

            gridView.disable();

        peditar.up('panel').getLayout().setActiveItem(peditar).loadRecord(uso);
    },

    borrarUinternet: function(id) {
        var request = {
            id: id,
            data: {
                borrado: true
            }
        };

        Ext.data.JsonP.request({
            url: BTC.app.base_url,
            params: {
                c: 'perfildemografico',
                m: 'editUsoInternet',
                request: JSON.stringify(request)
            },
            scope: this,
            success: function(response){
                if (response.success){
                    BTC.app.showDialog('Frecuencia de uso de internet  Eliminado', 'Se ha eliminado la frecuencia de uso de internet con ID:'+id+' de manera exitosa', 'xf087', 'right', false, 3000);
                    Ext.getStore('Usointernet').reload();
                } else {
                    Ext.Msg.alert('Error', response.response);
                }
            },
            failure: function(response){
                EBTC.app.showFatalError();
            }
        });
    },

    init: function(application) {
        this.control({
            "menu#UintContextMenu": {
                click: this.onMenuClick
            },
            "#PanelUinternet #BtnCancelar": {
                click: this.onExitEdition
            },
            "#PanelUinternet #BtnCrear": {
                click: this.onCreateNewUinternet
            },
            "#PanelUinternet #BtnGuardarCambios": {
                click: this.onGuardarCambiosUinternet
            }
        });
    }

});
