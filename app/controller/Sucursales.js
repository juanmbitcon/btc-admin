/*
 * File: app/controller/Sucursales.js
 *
 * This file was generated by Sencha Architect version 3.2.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('BTC.controller.Sucursales', {
    extend: 'Ext.app.Controller',
    alias: 'controller.sucursals',

    requires: [
        'Ext.window.MessageBox',
        'Ext.util.Point'
    ],

    refs: [
        {
            ref: 'verSucursales',
            selector: 'grid#VerSucursales'
        },
        {
            ref: 'editarPanel',
            selector: '#EditarPanel'
        }
    ],

    onMenuClick: function(menu, item, e, eOpts) {


        var ctrl = this,
            item = item.getItemId(),
            sucursal = this.getSelectedRecord();


        switch (item){
            case 'ver':
                ctrl.verDetallesSucursal(sucursal);
                break;

            case 'editar':
                ctrl.editarSucursal(sucursal);
                break;

            case 'eliminar':
                Ext.Msg.show({
                    title: '¿Desea continuar con el borrado?',
                    msg: 'Esta acción no se puede deshacer, ¿desea continuar con el borrado de la sucursal '+sucursal.get('nombre_sucursal')+'?',
                    buttonText: {yes: "Borrar",no: "Cancelar"},
                    icon: Ext.Msg.QUESTION,
                    fn: function(btn){
                        if (btn == 'yes'){
                            ctrl.borrarSucursal(sucursal.get('id'));
                        }
                    }
                });
                break;
        }
    },

    onGuardarCambios: function(button, e, eOpts) {
        var sucursal = this.getSelectedRecord(),
            form = button.up('form'),
            values = form.getForm().getValues(),
            verSuc = this.getVerSucursales(),
            request = {
                id: sucursal.get('id'),
                data: values
            };

        Ext.data.JsonP.request({
            url: BTC.app.base_url,
            params: {
                c: 'sucursal',
                m: 'editSucursal',
                request: JSON.stringify(request)
            },
            scope: this,
            success: function(response){
                if (response.success){
                    BTC.app.showDialog('Sucursal Editada', response.response, 'xf087', 'right', false, 3000);

                    form.getForm().reset();
                    button.up('window').close();
                    verSuc.getStore().reload();


                } else {
                    Ext.Msg.alert('Error', response.response);
                }
            },
            failure: function(response){
                BTC.app.showFatalError();
            }
        });


    },

    onCrearSucursal: function(button, e, eOpts) {
        var form = button.up('form'),
            verSuc = this.getVerSucursales(),
            values = form.getForm().getValues();

        Ext.data.JsonP.request({
            url: BTC.app.base_url,
            scope: this,
            params: {
                c: 'sucursal',
                m: 'createSucursal',
                request: JSON.stringify(values)
            },
            success: function(response){
                if (response.success){
                    BTC.app.showDialog('Sucursal Creada', response.response, 'xf087', 'right', false, 3000);
                    form.getForm().reset();
                    button.up('window').close();
                    verSuc.getStore().reload();
                } else {
                    Ext.Msg.alert('Error', response.response);
                }
            },
            failure: function(response){
                BTC.app.showFatalError();
            }
        });


    },

    onExitEdition: function(button, e, eOpts) {
        var form = button.up('form');

               form.getForm().reset();
               button.up('window').close();
    },

    onExitCrear: function(button, e, eOpts) {
        var form = button.up('form');

               form.getForm().reset();
               button.up('window').close();
    },

    onVerReportesSucursalesClick: function(item, e, eOpts) {
        Ext.create("widget.reporteador", {
            model: 'BTC.model.Sucursal',
            store: 'BTC.store.Sucursales',
            panel: 'BTC.view.VerSucursales'
        }).show();
    },

    onExportXLSSucursalesClick: function(item, e, eOpts) {
        var grid = item.up('grid'),
            store = grid.getView().getStore(),
            columnas = [],
            rows = [];


        if (store.getCount() > 0){

            Ext.each(store.model.getFields(), function(field){
                columnas.push(field.name);
            });

            store.each(function(record){
                var row = [];
                Ext.each(columnas, function(field){
                    var value = String(record.get(field));
                    row.push( (value !== 'null' ? value : '') );
                });
                rows.push(row);
            });

            var request = {
                columnas: columnas,
                detalles: {
                    rows: rows
                }
            };

            Ext.create('Ext.form.Panel', {
                url: BTC.app.base_url + '/export_excel/',
                method: 'POST',
                baseParams: {
                    request: encodeURIComponent(JSON.stringify(request))
                }
            }).submit({
                target: '_blank',
                standardSubmit: true
            });

        } else {
            Ext.Msg.alert('Alerta', 'El grid está vacío');
        }
    },

    onBtnActualizarSucursalesClick: function(button, e, eOpts) {
        var me = this;

        me.getVerSucursales().getStore().reload();
    },

    showView: function(view) {
        this.getDisplay().getLayout().setActiveItem(view);
    },

    showContextMenu: function(record, xy) {
        this.selectedRecord = record;
        // PERMISOS
        var sesion = BTC.app.sesion,
            perms = BTC.app.getController('Permisos'),
            vista = perms.findPermisoByName('VerSucursales');
        // PERMISOS

        var editar = false;
        var eliminar = false;

        if (!sesion.is_admin){
            if (parseInt(sesion.empleado.id_sucursal) === record.get('id')){
                editar = perms.findAccionByName(vista, 'editar');
                eliminar = perms.findAccionByName(vista, 'eliminar');
            }
        } else {
            editar = true;
            eliminar = true;
        }

        var menu = Ext.create('Ext.menu.Menu', {
            width: 120,
            bodyPadding: 3,
            autoDestroy: true,
            itemId: 'SucContextMenu',
            items: [{
                xtype: 'menuitem',
                glyph: 'xf06e@FontAwesome',
                itemId: 'ver',
                text: 'Ver'
            },{
                xtype: 'menuitem',
                glyph: 'xf044@FontAwesome',
                itemId: 'editar',
                disabled: !editar,
                text: 'Editar'
            },{
                xtype: 'menuseparator'
            },{
                xtype: 'menuitem',
                glyph: 'xf1f8@FontAwesome',
                itemId: 'eliminar',
                disabled: !eliminar,
                text: 'Eliminar'
            }]
        });

        menu.showAt(xy[0], xy[1]);
    },

    getSelectedRecord: function() {
        return this.selectedRecord;
    },

    verDetallesSucursal: function(sucursal) {
        var wdow = Ext.create('widget.sucursalverdetalles');

        wdow.down('form').getForm().loadRecord(sucursal);
        wdow.show();
    },

    editarSucursal: function(sucursal) {
        var wdow = Ext.create('widget.sucursaleditarnuevo', {
            title: 'Edición de Sucursal',
            activeItem: 1
        });

        var latlong = sucursal.get('ubicacion_geografica').split(',');
        //window.campo = wdow.down('#EditarSucursal #lat');
        wdow.down('#EditarSucursal #lat').setValue(latlong[0]);
        wdow.down('#EditarSucursal #log').setValue(latlong[1]);

        wdow.down('form#EditarSucursal').getForm().loadRecord(sucursal);
        wdow.show();
    },

    crearNuevo: function() {

        var wdow = Ext.create('widget.sucursaleditarnuevo', {
            title: 'Crear Nueva Sucursal',
            activeItem: 0
        });

        wdow.show();
    },

    borrarSucursal: function(id) {
        var request = {
            id: id,
            data: {
                borrado: true
            }
        };

        Ext.data.JsonP.request({
            url: BTC.app.base_url,
            params: {
                c: 'sucursal',
                m: 'editSucursal',
                request: JSON.stringify(request)
            },
            scope: this,
            success: function(response){
                if (response.success){
                    BTC.app.showDialog('Sucursal Eliminada', 'Se ha eliminado la sucursal con ID:'+id+' de manera exitosa', 'xf087', 'right', false, 3000);
                    Ext.getStore('Sucursales').reload();
                } else {
                    Ext.Msg.alert('Error', response.response);
                }
            },
            failure: function(response){
                EBTC.app.showFatalError();
            }
        });
    },

    acl_loadSucursales: function(borrado, callback) {
        var store = Ext.getStore('Sucursales'),
            sesion = BTC.app.sesion,
            borrado = (typeof borrado === 'undefined' ? false : borrado);

        var params = {
            borrado: borrado
        };

        if (!sesion.is_admin){
            params.request = JSON.stringify([{field: 'id', value: parseInt(sesion.empleado.id_sucursal)}]);
        }

        store.load({
            params: params,
            callback: function(records, operation, success){
                if (typeof callback !== 'undefined' && callback !== null){
                    callback();
                }
            }
        });
    },

    init: function(application) {
        this.control({
            "menu#SucContextMenu": {
                click: this.onMenuClick
            },
            "#SucursalEditarNuevo #BtnGuardarCambios": {
                click: this.onGuardarCambios
            },
            "#SucursalEditarNuevo #BtnCrear": {
                click: this.onCrearSucursal
            },
            "#EditarSucursal #BtnCancelar": {
                click: this.onExitEdition
            },
            "#NuevaSucursal #BtnCancelar": {
                click: this.onExitCrear
            },
            "#VerReportesSucursales": {
                click: this.onVerReportesSucursalesClick
            },
            "#ExportXLSSucursales": {
                click: this.onExportXLSSucursalesClick
            },
            "#BtnActualizarSucursales": {
                click: this.onBtnActualizarSucursalesClick
            }
        });
    }

});
