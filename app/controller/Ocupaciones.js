/*
 * File: app/controller/Ocupaciones.js
 *
 * This file was generated by Sencha Architect version 3.2.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('BTC.controller.Ocupaciones', {
    extend: 'Ext.app.Controller',

    refs: [
        {
            ref: 'panelOcupacion',
            selector: '#PanelOcupacion'
        }
    ],

    onMenuClick: function(menu, item, e, eOpts) {
        var ctrl = this,
            item = item.getItemId(),
            ocupacion = this.getSelectedRecord();


        switch (item){
             case 'editar':
               ctrl.editarOcupacion(ocupacion);
                break;

            case 'eliminar':
                Ext.Msg.show({
                    title: '¿Desea continuar con el borrado?',
                    msg: 'Esta acción no se puede deshacer, ¿desea continuar con el borrado de la ocupacion '+ocupacion.get('nombre')+'?',
                    buttonText: {yes: "Borrar",no: "Cancelar"},
                    icon: Ext.Msg.QUESTION,
                    fn: function(btn){
                        if (btn == 'yes'){
                            ctrl.borrarOcupacion(ocupacion.get('id'));
                        }
                    }
                });
                break;
        }
    },

    onExitEdition: function(button, e, eOpts) {
        var form = button.up('form'),
            panelOcupacion = this.getPanelOcupacion(),
            pnuevo = panelOcupacion.down('#PanelNuevo'),
            gridView =form.up('grid').getView();


            form.getForm().reset();
            gridView.enable();
            pnuevo.up('panel').getLayout().setActiveItem(pnuevo);

    },

    onCreateNewOcupacion: function(button, e, eOpts) {
        var form = button.up('form'),
            grid = button.up('grid'),
            values = form.getForm().getValues();

        Ext.data.JsonP.request({
            url: BTC.app.base_url,
            scope: this,
            params: {
                c: 'perfildemografico',
                m: 'createOcupacion',
                request: JSON.stringify(values)
            },
            success: function(response){
                if (response.success){
                    BTC.app.showDialog('Ocupacion Creado', response.response, 'xf087', 'right', false, 3000);
                    form.getForm().reset();
                    grid.getStore().reload();
                } else {
                    Ext.Msg.alert('Error', response.response);
                }
            },
            failure: function(response){
                BTC.app.showFatalError();
            }
        });
    },

    onGuardarCambiosOcupacion: function(button, e, eOpts) {
        var ocupacion = this.getSelectedRecord(),
            form = button.up('form'),
            values = form.getForm().getValues(),
            grid = button.up('grid'),
            panelOcu = this.getPanelOcupacion(),
            pnuevo = panelOcu.down('#PanelNuevo'),
            request = {
                id: ocupacion.get('id'),
                data: values
            };

        Ext.data.JsonP.request({
            url: BTC.app.base_url,
            params: {
                c: 'perfildemografico',
                m: 'editOcupacion',
                request: JSON.stringify(request)
            },
            scope: this,
            success: function(response){
                if (response.success){
                    BTC.app.showDialog('Ocupacion Editado', response.response, 'xf087', 'right', false, 3000);
                    grid.getStore().reload();
                    form.getForm().reset();
                    pnuevo.up('panel').getLayout().setActiveItem(pnuevo);

                } else {
                    Ext.Msg.alert('Error', response.response);
                }
            },
            failure: function(response){
                BTC.app.showFatalError();
            }
        });
    },

    showContextMenu: function(record, xy) {
        this.selectedRecord = record;

        var menu = Ext.create('Ext.menu.Menu', {
            width: 120,
            bodyPadding: 3,
            autoDestroy: true,
            itemId: 'OcuContextMenu',
            items: [{
                xtype: 'menuitem',
                glyph: 'xf044@FontAwesome',
                itemId: 'editar',
                //disabled: !perms.findAccionByName(vista, 'editar'),
                text: 'Editar'
            },{
                xtype: 'menuseparator'
            },{
                xtype: 'menuitem',
                glyph: 'xf1f8@FontAwesome',
                itemId: 'eliminar',
                //disabled: !perms.findAccionByName(vista, 'eliminar'),
                text: 'Eliminar'
            }]
        });

        menu.showAt(xy[0], xy[1]);
    },

    getSelectedRecord: function() {
        return this.selectedRecord;
    },

    editarOcupacion: function(ocupacion) {
        var pocupacion = this.getPanelOcupacion(),
            peditar = pocupacion.down('#PanelEditar'),
            gridView = peditar.up('grid').getView();

            gridView.disable();

        peditar.up('panel').getLayout().setActiveItem(peditar).loadRecord(ocupacion);
    },

    borrarOcupacion: function(id) {
        var request = {
            id: id,
            data: {
                borrado: true
            }
        };

        Ext.data.JsonP.request({
            url: BTC.app.base_url,
            params: {
                c: 'perfildemografico',
                m: 'editOcupacion',
                request: JSON.stringify(request)
            },
            scope: this,
            success: function(response){
                if (response.success){
                    BTC.app.showDialog('Ocupacion Eliminado', 'Se ha eliminado la ocupacion con ID:'+id+' de manera exitosa', 'xf087', 'right', false, 3000);
                    Ext.getStore('Ocupaciones').reload();
                } else {
                    Ext.Msg.alert('Error', response.response);
                }
            },
            failure: function(response){
                EBTC.app.showFatalError();
            }
        });
    },

    init: function(application) {
        this.control({
            "menu#OcuContextMenu": {
                click: this.onMenuClick
            },
            "#PanelOcupacion #BtnCancelar": {
                click: this.onExitEdition
            },
            "#PanelOcupacion #BtnCrear": {
                click: this.onCreateNewOcupacion
            },
            "#PanelOcupacion #BtnGuardarCambios": {
                click: this.onGuardarCambiosOcupacion
            }
        });
    }

});
