/*
 * File: app/controller/Navegacion.js
 *
 * This file was generated by Sencha Architect version 3.2.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('BTC.controller.Navegacion', {
    extend: 'Ext.app.Controller',

    activeItem: 'null',

    refs: [
        {
            ref: 'contentPanel',
            selector: '#contentPanel'
        }
    ],

    onMenuClick: function(item, e, eOpts) {
        var panel = '#'+item.getItemId(),
            contentPanel = this.getContentPanel();

        panel = contentPanel.down(panel);

        contentPanel.layout.setActiveItem(panel); // Cambia el item activo

        var currentActive = this.getActiveMenuitem(); // Obtiene el item activo actualmente

        /*if (currentActive !== null){
            currentActive.setActive(false);
        }

        item.setActive(true);*/
        this.activeItem = item; // Nuevo item activo
    },

    getActiveMenuitem: function() {
        return this.activeItem;
    },

    init: function(application) {
        this.control({
            "#menuPanel menuitem": {
                click: this.onMenuClick
            }
        });
    }

});
