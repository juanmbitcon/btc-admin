/*
 * File: app/store/Grupos.js
 *
 * This file was generated by Sencha Architect version 3.2.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('BTC.store.Grupos', {
    extend: 'Ext.data.Store',

    requires: [
        'BTC.model.Grupo',
        'Ext.data.proxy.JsonP',
        'Ext.data.reader.Json'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            autoLoad: false,
            model: 'BTC.model.Grupo',
            storeId: 'Grupos',
            proxy: {
                type: 'jsonp',
                extraParams: {
                    c: 'usuarios',
                    m: 'getGroups'
                },
                reader: {
                    type: 'json',
                    root: 'response.groups',
                    totalProperty: 'response.total'
                }
            },
            listeners: {
                beforeload: {
                    fn: me.onJsonpstoreBeforeLoad,
                    scope: me
                }
            }
        }, cfg)]);
    },

    onJsonpstoreBeforeLoad: function(store, operation, eOpts) {
        store.getProxy().url = BTC.app.base_url;
    }

});