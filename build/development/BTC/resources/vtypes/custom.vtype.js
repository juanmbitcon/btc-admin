Ext.onReady(function(){
	Ext.apply(Ext.form.field.VTypes, {
	    CustomNumber:  function(v) {
	        return /^\+?([0-9]\d*)$/i.test(v);
	    },
	    CustomNumberText: 'El valor debe ser numérico, entero y positivo.',
	    CustomNumberMask: /[0-9]/i
	});
});