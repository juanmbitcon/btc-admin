# ext-theme-neptune-2cd6afa5-5198-449f-92a0-98a720d61e97/sass/etc

This folder contains miscellaneous SASS files. Unlike `"ext-theme-neptune-2cd6afa5-5198-449f-92a0-98a720d61e97/sass/etc"`, these files
need to be used explicitly.
