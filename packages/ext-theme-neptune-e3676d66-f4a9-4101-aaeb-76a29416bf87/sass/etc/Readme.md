# ext-theme-neptune-e3676d66-f4a9-4101-aaeb-76a29416bf87/sass/etc

This folder contains miscellaneous SASS files. Unlike `"ext-theme-neptune-e3676d66-f4a9-4101-aaeb-76a29416bf87/sass/etc"`, these files
need to be used explicitly.
